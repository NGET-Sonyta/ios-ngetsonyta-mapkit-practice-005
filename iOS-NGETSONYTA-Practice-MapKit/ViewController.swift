

import UIKit
import MapKit

class ViewController: UIViewController{

    @IBOutlet weak var mapKit: MKMapView!
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var streetName: UILabel!
    let locationManager = CLLocationManager()
    var previousLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapKit.delegate = self
        if CLLocationManager.locationServicesEnabled(){
            setUpLocationManager()
            checkLocationAuthorization()
        }
   
    }
    
    @IBAction func trackMe(_ sender: UIButton) {
        checkLocationAuthorization()
    }
  
}

extension ViewController: CLLocationManagerDelegate, MKMapViewDelegate{
    
    func setUpLocationManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func startTrackingUserLocation() {
        self.mapKit.showsUserLocation = true
        centerViewOnUserLocation()
        self.locationManager.startUpdatingLocation()
        self.previousLocation = getCenterLocation(mapView: mapKit)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        let center = getCenterLocation(mapView: mapKit)
        guard let previousLocation = self.previousLocation else {
                return
        }
        guard center.distance(from: previousLocation) > 50 else {
            return
        }
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(center) { (placeMark, error) in
            if let error = error{
                print(error.localizedDescription)
                return
            }
            if let placeMark = placeMark?.first{
                self.streetName.text = placeMark.name ?? ""
                self.cityName.text = placeMark.country ?? ""
            }
        }
    }
    
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate{
            let regoin = MKCoordinateRegion(center: location, latitudinalMeters: 500, longitudinalMeters: 500)
            self.mapKit.setRegion(regoin, animated: true)        }
    }
    
    func getCenterLocation(mapView: MKMapView) -> CLLocation {
        let latitude = mapView.centerCoordinate.latitude
        let longitude = mapView.centerCoordinate.longitude
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            startTrackingUserLocation()
            break
        case .denied:
            locationManager.requestWhenInUseAuthorization()
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        default:
            break
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
    
}
